package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;

import model.Entity;
import model.Transfer;
import view.PaginalComponent;

public class TransferControl implements TabController {
    private PaginalComponent pagComp;

    public TransferControl(PaginalComponent pagComp) {
        this.pagComp = pagComp;
    }

    @Override
    public void add() {
        new AddDialog();
    }

    @Override
    public void update() {
        // do nothing
    }

    @Override
    public void delete() {
        new DeleteDialog();
    }

    @Override
    public List<Entity> getList() {
        List<Entity> list = new ArrayList<Entity>();
        Transfer transfer;
        ResultSet rs = Connector.INSTANCE
                .executeQuery("SELECT * FROM transfer ORDER BY transfer_date");
        try {
            while (rs.next()) {
                transfer = new Transfer();
                transfer.setTransferDate(rs.getDate("transfer_date"));
                transfer.setNumber(rs.getInt("number"));
                transfer.setPatientID(rs.getInt("id_patient"));
                list.add(transfer);
            }
        } catch (SQLException e) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null,
                    e);
        }
        return list;
    }
    
    @Override
    public void updateTable() {
        pagComp.updateTable(getList());
    }

    @SuppressWarnings("serial")
    private class AddDialog extends JDialog {

        public AddDialog() {
            setTitle("Add the transfer");
            setModal(true);
            setSize(200, 190);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            setLocationRelativeTo(null);

            add(new JLabel("Transfer date(yyyy-mm-dd):"));
            JFormattedTextField dateField = new JFormattedTextField(
                    new SimpleDateFormat("yyyy-mm-dd"));
            add(dateField);
            add(new JLabel("Ward number:"));
            JTextField numberField = new JTextField();
            add(numberField);
            add(new JLabel("Patient ID"));
            JTextField pidField = new JTextField();
            add(pidField);
            JButton button = new JButton("Add the transfer");
            add(button);

            button.addActionListener((e) -> {
                String sql = "INSERT INTO transfer( transfer_date, \"number\", id_patient) VALUES ('"
                        + dateField.getText()
                        + "', "
                        + numberField.getText()
                        + ", " + pidField.getText() + ");";
                Connector.INSTANCE.executeUpdate(sql);
                updateTable();
                this.dispose();
            });

            setVisible(true);
        }
    }

//    @SuppressWarnings("serial")
//    private class UpdateDialog extends JDialog {
//
//        public UpdateDialog() {
//            setTitle("Update the ward");
//            setModal(true);
//            setSize(200, 150);
//            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
//            setLocationRelativeTo(null);
//
//            add(new JLabel("Ward number:"));
//            JTextField numberField = new JTextField();
//            add(numberField);
//            add(new JLabel("Ward phone:"));
//            JTextField phoneField = new JTextField();
//            add(phoneField);
//            JButton button = new JButton("Update the ward");
//            add(button);
//
//            button.addActionListener((e) -> {
//                String sql = "UPDATE transfer SET transfer_date=?, \"number\"=?, id_patient=? WHERE <condition>;";
//                Connector.INSTANCE.executeUpdate(sql);
//                updateTable();
//                this.dispose();
//            });
//
//            setVisible(true);
//        }
//    }

    @SuppressWarnings("serial")
    private class DeleteDialog extends JDialog {

        public DeleteDialog() {
            setTitle("Delete the transfer");
            setModal(true);
            setSize(200, 190);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            setLocationRelativeTo(null);

            add(new JLabel("Transfer date(yyyy-mm-dd):"));
            JFormattedTextField dateField = new JFormattedTextField(
                    new SimpleDateFormat("yyyy-mm-dd"));
            add(dateField);
            add(new JLabel("Ward number:"));
            JTextField numberField = new JTextField();
            add(numberField);
            add(new JLabel("Patient ID"));
            JTextField pidField = new JTextField();
            add(pidField);
            JButton button = new JButton("Delete the transfer");
            add(button);

            button.addActionListener((e) -> {
                String sql = "DELETE FROM transfer WHERE transfer_date='"
                        + dateField.getText()
                        + "' AND number=" + numberField.getText() + " AND id_patient=" + pidField.getText() + ";";
                Connector.INSTANCE.execute(sql);
                updateTable();
                this.dispose();
            });

            setVisible(true);
        }
    }
}
