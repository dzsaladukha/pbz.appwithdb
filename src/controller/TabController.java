package controller;

import java.util.List;

import model.Entity;

public interface TabController {
    public void add();
    public void update();
    public void delete();
    public List<Entity> getList();
    public void updateTable();
}
