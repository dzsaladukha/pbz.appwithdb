package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import model.Entity;
import controller.PaginalComponentController;

@SuppressWarnings("serial")
public class PaginalComponent extends JPanel {

    private JTable table = new JTable();
    private JPanel controlPanel = new JPanel();
    private List<Entity> itemList;
    public JLabel totalElem;
    public JLabel pageNum;
    public JTextField numOnPage;
    private Object[] columnIdentifiers;

    public PaginalComponent(List<Entity> itemList, Object[] columnIdentifiers) {

        this.itemList = itemList;
        this.columnIdentifiers = columnIdentifiers;

        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(650, 250));

        add(loadContent(), BorderLayout.CENTER);
        add(createControlPanel(), BorderLayout.SOUTH);
    }

    public PaginalComponent() {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(650, 250));
    }
    
    public void init(){
        add(loadContent(), BorderLayout.CENTER);
        add(createControlPanel(), BorderLayout.SOUTH);
    }

    private JPanel createControlPanel() {
        controlPanel.setLayout(new FlowLayout());
        JButton first = new JButton("<<");
        JButton prev = new JButton("<");
        JButton next = new JButton(">");
        JButton last = new JButton(">>");
        String text = new String(new Integer(itemList.size()).toString());
        numOnPage = new JTextField(text, 2);
        totalElem = new JLabel("из " + text);
        pageNum = new JLabel("Страница 1");

        final PaginalComponentController controller = new PaginalComponentController(
                this);
        first.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.toFirstPage();
            }

        });
        prev.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.toPrevPage();
            }

        });
        next.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.toNextPage();
            }

        });
        last.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.toLastPage();
            }

        });
        numOnPage.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.changeCountOfVisibleElem();
            }

        });

        controlPanel.add(first);
        controlPanel.add(prev);
        controlPanel.add(pageNum);
        controlPanel.add(next);
        controlPanel.add(last);
        controlPanel.add(numOnPage);
        controlPanel.add(totalElem);
        return controlPanel;
    }

    private JScrollPane loadContent() {
        DefaultTableModel items = new DefaultTableModel();
        this.table.setModel(items);
        JScrollPane scroll = new JScrollPane(table);

        items.setColumnIdentifiers(columnIdentifiers);

        for (int index = 0; index < itemList.size(); index++) {
            items.addRow(itemList.get(index).toRow());
        }
        this.table.setEnabled(false);

        return scroll;
    }

    public void updateTable() {
        DefaultTableModel items = new DefaultTableModel();
        items.setColumnIdentifiers(columnIdentifiers);
        for (int index = 0; index < itemList.size(); index++) {
            items.addRow(itemList.get(index).toRow());
        }
        this.table.setModel(items);
        this.table.updateUI();

        String text = new String((new Integer(itemList.size()).toString()));
        numOnPage.setText(text);
        totalElem.setText("из " + text);
        pageNum.setText("Страница 1");
    }

    public void updateTable(List<Entity> libr) {
        DefaultTableModel items = new DefaultTableModel();
        items.setColumnIdentifiers(columnIdentifiers);
        for (int index = 0; index < libr.size(); index++) {
            items.addRow(libr.get(index).toRow());
        }
        this.table.setModel(items);
        this.table.updateUI();

        numOnPage.setText(new String((new Integer(libr.size()).toString())));
        totalElem.setText("из "
                + new String((new Integer(itemList.size()).toString())));
    }

    public List<Entity> getItemList() {
        return itemList;
    }

    public void setColumnIdentifiers(Object[] columnIdentifiers) {
        this.columnIdentifiers = columnIdentifiers;
    }

    public void setItemList(List<Entity> list) {
        this.itemList = list;
    }
}
