package view;

import controller.WardControl;

@SuppressWarnings("serial")
public class WardPane extends Tab {
    
    public WardPane(){
        this.columnIdentifiers = new Object[] {"Number", "Phone"};
        this.tabControl = new WardControl(pagComp);
        setPagCompAndControlPane();
    }
}
