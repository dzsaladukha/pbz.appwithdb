package view;

import controller.PatientControl;

@SuppressWarnings("serial")
public class PatientPane extends Tab {

    public PatientPane() {
        this.columnIdentifiers = new Object[] { "PID", "Entering date",
                "Entering way", "Fullname", "Age", "Sex", "Approximate age",
                "Approximate height", "Hair color", "Special signs",
                "Leaving date", "Leaving reason", "Ward number"};
        this.tabControl = new PatientControl(pagComp);
        setPagCompAndControlPane();
    }
}
