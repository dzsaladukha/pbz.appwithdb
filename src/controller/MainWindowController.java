package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;

import model.Entity;
import model.Ward;
import view.PaginalComponent;

public class MainWindowController {

    public void requestWard() {
        new RequestWardDialog();
    }

    public void requestListOfPatient() {
        new RLOPD();
    }

    public void requestListOfFemale() {
        new RLOF();
    }

    public void completeTreatment() {
        new CompleteTreatmentDialog();
    }

    private void callResultDialog(Object[] objects, List<Entity> list) {
        new ResultFrame(objects, list);
    }

    @SuppressWarnings("serial")
    private class RequestWardDialog extends JDialog {

        public RequestWardDialog() {
            setTitle("Request ward information");
            setModal(true);
            setSize(200, 105);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            setLocationRelativeTo(null);

            add(new JLabel("Patietn ID:"));
            JTextField pidField = new JTextField();
            add(pidField);

            JButton button = new JButton("request");
            add(button);

            button.addActionListener((e) -> {
                String sql = "SELECT number,phone FROM ward WHERE number in (SELECT ward_number FROM patient WHERE "
                        + pidField.getText() + "=id_patient );";
                List<Entity> list = new ArrayList<Entity>();
                Ward ward;
                ResultSet rs = Connector.INSTANCE.executeQuery(sql);
                try {
                    while (rs.next()) {
                        ward = new Ward();
                        ward.setNumber(rs.getInt("number"));
                        ward.setPhone(rs.getString("phone"));
                        list.add(ward);
                    }
                } catch (SQLException e1) {
                    Logger.getLogger(Connector.class.getName()).log(
                            Level.SEVERE, null, e1);
                }
                callResultDialog(new Object[] { "Number", "Phone" }, list);
                this.dispose();
            });

            setVisible(true);
        }
    }

    @SuppressWarnings("serial")
    private class CompleteTreatmentDialog extends JDialog {

        public CompleteTreatmentDialog() {
            setTitle("Complete treatment");
            setModal(true);
            setSize(200, 195);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            setLocationRelativeTo(null);

            add(new JLabel("Patietn ID:"));
            JTextField pidField = new JTextField();
            add(pidField);
            add(new JLabel("Leaving date(yyyy-mm-dd):"));
            JFormattedTextField ldField = new JFormattedTextField(
                    new SimpleDateFormat("yyyy-mm-dd"));
            add(ldField);
            add(new JLabel("Leaving reason:"));
            JTextField lrField = new JTextField();
            add(lrField);

            JButton button = new JButton("Complete treatment");
            add(button);

            button.addActionListener((e) -> {
                Connector.INSTANCE
                        .executeUpdate("UPDATE patient SET leave_date='"
                                + ldField.getText() + "',  leave_reason='"
                                + lrField.getText() + "' WHERE id_patient="
                                + pidField.getText() + ";");
                this.dispose();
            });

            setVisible(true);
        }
    }

    @SuppressWarnings("serial")
    private class RLOF extends JDialog {

        public RLOF() {
            setTitle("Request list of female patient on specified date");
            setModal(true);
            setSize(250, 150);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            setLocationRelativeTo(null);

            add(new JLabel("Specified date(yyyy-mm-dd):"));
            JFormattedTextField curField = new JFormattedTextField(
                    new SimpleDateFormat("yyyy-mm-dd"));
            add(curField);
            add(new JLabel("Age:"));
            JTextField ageField = new JTextField();
            add(ageField);

            JButton button = new JButton("request");
            add(button);

            button.addActionListener((e) -> {
                String sql = "SELECT surname, firstname, patronymic, age FROM patient WHERE (entr_date<'"
                        + curField.getText()
                        + "' AND (leave_reason='on treatment' OR leave_date>'"
                        + curField.getText()
                        + "')) AND sex='female' AND age>="
                        + ageField.getText() + ";";
                List<Entity> list = new ArrayList<Entity>();
                RLOFdata rlof;
                ResultSet rs = Connector.INSTANCE.executeQuery(sql);
                try {
                    while (rs.next()) {
                        rlof = new RLOFdata();
                        rlof.setAge(rs.getInt("age"));
                        rlof.setSurname(rs.getString("surname"));
                        rlof.setFirstname(rs.getString("firstname"));
                        rlof.setPatronymic(rs.getString("patronymic"));
                        rlof.setCurDate(curField.getText());
                        list.add(rlof);
                    }
                } catch (SQLException e1) {
                    Logger.getLogger(Connector.class.getName()).log(
                            Level.SEVERE, null, e1);
                }
                callResultDialog(new Object[] { "Specified date", "Fullname",
                        "Age" }, list);
                this.dispose();
            });

            setVisible(true);
        }
    }

    private class RLOFdata implements Entity {
        private String surname;
        private String firstname;
        private String patronymic;
        private Integer age;
        private String curr_date;

        public void setCurDate(String text) {
            this.curr_date = text;
        }

        public void setPatronymic(String string) {
            this.patronymic = string;
        }

        public void setFirstname(String string) {
            this.firstname = string;
        }

        public void setSurname(String string) {
            this.surname = string;
        }

        public void setAge(int int1) {
            this.age = int1;
        }

        @Override
        public String[] toRow() {
            String fullname = null, ageStr = null;

            if (surname != null) {
                fullname = surname + " ";
            }
            if (firstname != null) {
                fullname += firstname + " ";
            }
            if (patronymic != null) {
                fullname += patronymic;
            }
            if (age != null) {
                if (age > 0) {
                    ageStr = age.toString();
                }
            }

            return new String[] { curr_date, fullname, ageStr };
        }
    }

    @SuppressWarnings("serial")
    private class RLOPD extends JDialog {

        public RLOPD() {
            setTitle("Request list of patient on specified date");
            setModal(true);
            setSize(250, 105);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            setLocationRelativeTo(null);

            add(new JLabel("Specified date(yyyy-mm-dd):"));
            JFormattedTextField curField = new JFormattedTextField(
                    new SimpleDateFormat("yyyy-mm-dd"));
            add(curField);

            JButton button = new JButton("request");
            add(button);

            button.addActionListener((e) -> {
                String sql = "SELECT ward_number, surname, firstname, patronymic, age, entr_date FROM patient WHERE entr_date<'"
                        + curField.getText()
                        + "' AND (leave_reason='on treatment' OR leave_date>'"
                        + curField.getText() + "');";
                List<Entity> list = new ArrayList<Entity>();
                RLOPDdata rlopd;
                ResultSet rs = Connector.INSTANCE.executeQuery(sql);
                try {
                    while (rs.next()) {
                        rlopd = new RLOPDdata();
                        rlopd.setWardNumber(rs.getInt("ward_number"));
                        rlopd.setAge(rs.getInt("age"));
                        rlopd.setEntrDate(rs.getString("entr_date"));
                        rlopd.setSurname(rs.getString("surname"));
                        rlopd.setFirstname(rs.getString("firstname"));
                        rlopd.setPatronymic(rs.getString("patronymic"));
                        rlopd.setCurDate(curField.getText());
                        list.add(rlopd);
                    }
                } catch (SQLException e1) {
                    Logger.getLogger(Connector.class.getName()).log(
                            Level.SEVERE, null, e1);
                }
                callResultDialog(new Object[] { "Specified date", "Fullname",
                        "Entering date", "Age", "Ward number" }, list);
                this.dispose();
            });

            setVisible(true);
        }
    }

    private class RLOPDdata implements Entity {
        private Integer ward_number;
        private String entr_date;
        private String surname;
        private String firstname;
        private String patronymic;
        private Integer age;
        private String curr_date;

        @Override
        public String[] toRow() {
            String fullname = null, ageStr = null, ward_numberStr = null;

            if (surname != null) {
                fullname = surname + " ";
            }
            if (firstname != null) {
                fullname += firstname + " ";
            }
            if (patronymic != null) {
                fullname += patronymic;
            }
            if (age != null) {
                if (age > 0) {
                    ageStr = age.toString();
                }
            }
            if (ward_number != null) {
                if (ward_number > 0) {
                    ward_numberStr = ward_number.toString();
                }
            }

            return new String[] { curr_date, fullname, entr_date, ageStr,
                    ward_numberStr };
        }

        public void setCurDate(String text) {
            this.curr_date = text;
        }

        public void setPatronymic(String string) {
            this.patronymic = string;
        }

        public void setFirstname(String string) {
            this.firstname = string;
        }

        public void setSurname(String string) {
            this.surname = string;
        }

        public void setEntrDate(String string) {
            this.entr_date = string;
        }

        public void setAge(int int1) {
            this.age = int1;
        }

        public void setWardNumber(int int1) {
            this.ward_number = int1;
        }
    }

    @SuppressWarnings("serial")
    private class ResultFrame extends JDialog {

        public ResultFrame(Object[] columnIdentifiers, List<Entity> list) {
            setTitle("Result");
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

            PaginalComponent pgComp = new PaginalComponent();
            pgComp.setColumnIdentifiers(columnIdentifiers);
            pgComp.setItemList(list);
            pgComp.init();
            add(pgComp);

            pack();
            setVisible(true);
        }
    }
}
