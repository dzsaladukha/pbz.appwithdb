package view;

import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import controller.MainWindowController;

public class MainWindow {
    private JFrame mainFrame;

    public MainWindow() {
        mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Toolkit tKit = Toolkit.getDefaultToolkit();
        mainFrame.setSize((int) tKit.getScreenSize().getWidth() / 8 * 7,
                (int) tKit.getScreenSize().getHeight() / 8 * 5);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setTitle("Hospital");
        mainFrame.setLayout(new GridLayout(1, 1));

        JTabbedPane tabbedPane = new JTabbedPane();
        addTabs(tabbedPane);

        JMenuBar menuBar = new JMenuBar();
        createMenu(menuBar);

        mainFrame.setJMenuBar(menuBar);
        mainFrame.add(tabbedPane);
        mainFrame.setVisible(true);
    }

    private void createMenu(JMenuBar menuBar) {
        JMenu menu = new JMenu("Requests");
        JMenuItem wardRequest = new JMenuItem("Show ward of specified patient"), listOfPatient = new JMenuItem(
                "Show list of patient on specified date"), listOfFemale = new JMenuItem(
                "Show list of female patient reached specified age on current date"), completeTreatment = new JMenuItem(
                "Complete treatment");

        MainWindowController cont = new MainWindowController();
        wardRequest.addActionListener((e) -> {
            cont.requestWard();
        });
        listOfPatient.addActionListener((e) -> {
            cont.requestListOfPatient();
        });
        listOfFemale.addActionListener((e) -> {
            cont.requestListOfFemale();
        });
        completeTreatment.addActionListener(e -> {
            cont.completeTreatment();
        });

        menu.add(wardRequest);
        menu.add(listOfPatient);
        menu.add(listOfFemale);
        menu.add(completeTreatment);
        menuBar.add(menu);
    }

    private void addTabs(JTabbedPane tabbedPane) {
        JPanel patientPane = new PatientPane();
        JPanel transferPane = new TransferPane();
        JPanel wardPane = new WardPane();

        tabbedPane.addTab("Patients", patientPane);
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
        tabbedPane.addTab("Wards", wardPane);
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
        tabbedPane.addTab("Transfers", transferPane);
        tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
    }
}
