package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;

import model.Entity;
import model.Patient;
import view.PaginalComponent;

public class PatientControl implements TabController {
    private PaginalComponent pagComp;

    public PatientControl(PaginalComponent pagComp) {
        this.pagComp = pagComp;
    }

    @Override
    public void add() {
        new AddDialog();
    }

    @Override
    public void update() {
        new UpdateDialog();
    }

    @Override
    public void delete() {
        new DeleteDialog();
    }

    @Override
    public List<Entity> getList() {
        List<Entity> list = new ArrayList<Entity>();
        Patient patient;
        ResultSet rs = Connector.INSTANCE
                .executeQuery("SELECT * FROM patient ORDER BY id_patient");
        try {
            while (rs.next()) {
                patient = new Patient();
                patient.setPatientID(rs.getInt("id_patient"));
                patient.setEntrDate(rs.getString("entr_date"));
                patient.setEntrWay(rs.getString("entr_way"));
                patient.setSurname(rs.getString("surname"));
                patient.setFirstname(rs.getString("firstname"));
                patient.setPatronymic(rs.getString("patronymic"));
                patient.setAge(rs.getInt("age"));
                patient.setSex(rs.getString("sex"));
                patient.setApprAge(rs.getInt("appr_age"));
                patient.setApprHeight(rs.getInt("appr_height"));
                patient.setHairColor(rs.getString("hair_color"));
                patient.setLeaveDate(rs.getString("leave_date"));
                patient.setLeaveReason(rs.getString("leave_reason"));
                patient.setWardNumber(rs.getInt("ward_number"));
                patient.setSpecialSigns(rs.getString("special_signs"));
                list.add(patient);
            }
        } catch (SQLException e) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null,
                    e);
        }
        return list;
    }

    @Override
    public void updateTable() {
        pagComp.updateTable(getList());
    }

    @SuppressWarnings("serial")
    private class AddDialog extends JDialog {

        public AddDialog() {
            setTitle("Add the patient");
            setModal(true);
            setSize(250, 570);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            setLocationRelativeTo(null);

            add(new JLabel("Entering date(yyyy-mm-dd)*:"));
            JFormattedTextField entrDateField = new JFormattedTextField(
                    new SimpleDateFormat("yyyy-mm-dd"));
            add(entrDateField);
            add(new JLabel("Entering way*:"));
            JTextField entrWayField = new JTextField();
            add(entrWayField);
            add(new JLabel("Surname:"));
            JTextField surnameField = new JTextField();
            add(surnameField);
            add(new JLabel("Firstname:"));
            JTextField firstnameField = new JTextField();
            add(firstnameField);
            add(new JLabel("Patronymic:"));
            JTextField patrField = new JTextField();
            add(patrField);
            add(new JLabel("Age:"));
            JTextField ageField = new JTextField();
            add(ageField);
            add(new JLabel("Sex:"));
            JTextField sexField = new JTextField();
            add(sexField);
            add(new JLabel("Approximate age:"));
            JTextField apprAgeField = new JTextField();
            add(apprAgeField);
            add(new JLabel("Approximate height:"));
            JTextField apprHeightField = new JTextField();
            add(apprHeightField);
            add(new JLabel("Hair color:"));
            JTextField hairField = new JTextField();
            add(hairField);
            add(new JLabel("Special signs:"));
            JTextField ssField = new JTextField();
            add(ssField);
            JButton button = new JButton("Add the patient");
            add(button);

            button.addActionListener((e) -> {
                String sql = "INSERT INTO  patient( entr_date, entr_way";
                String values = ") VALUES ('" + entrDateField.getText()
                        + "', '" + entrWayField.getText() + "'";
                if (!surnameField.getText().isEmpty()) {
                    sql += ", surname";
                    values += ", '" + surnameField.getText() + "'";
                }
                if (!firstnameField.getText().isEmpty()) {
                    sql += ", firstname";
                    values += ", '" + firstnameField.getText() + "'";
                }
                if (!patrField.getText().isEmpty()) {
                    sql += ", patronymic";
                    values += ", '" + patrField.getText() + "'";
                }
                if (!ageField.getText().isEmpty()) {
                    sql += ", age";
                    values += ", " + ageField.getText();
                }
                if (!sexField.getText().isEmpty()) {
                    sql += ", sex";
                    values += ", '" + sexField.getText() + "'";
                }
                if (!apprAgeField.getText().isEmpty()) {
                    sql += ", appr_age";
                    values += ", " + apprAgeField.getText();
                }
                if (!apprHeightField.getText().isEmpty()) {
                    sql += ", appr_height";
                    values += ", " + apprHeightField.getText();
                }
                if (!hairField.getText().isEmpty()) {
                    sql += ", hair_color";
                    values += ", '" + hairField.getText() + "'";
                }
                if (!ssField.getText().isEmpty()) {
                    sql += ", special_signs";
                    values += ", '" + ssField.getText() + "'";
                }
                Connector.INSTANCE.executeUpdate(sql + values + ");");
                updateTable();
                this.dispose();
            });

            setVisible(true);
        }
    }

    @SuppressWarnings("serial")
    private class UpdateDialog extends JDialog {

        public UpdateDialog() {
            setTitle("Update the patient");
            setModal(true);
            setSize(250, 570);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            setLocationRelativeTo(null);

            add(new JLabel("Patietn ID*:"));
            JTextField pidField = new JTextField();
            add(pidField);
            add(new JLabel("Entering date(yyyy-mm-dd):"));
            JFormattedTextField entrDateField = new JFormattedTextField(
                    new SimpleDateFormat("yyyy-mm-dd"));
            add(entrDateField);
            add(new JLabel("Entering way:"));
            JTextField entrWayField = new JTextField();
            add(entrWayField);
            add(new JLabel("Surname:"));
            JTextField surnameField = new JTextField();
            add(surnameField);
            add(new JLabel("Firstname:"));
            JTextField firstnameField = new JTextField();
            add(firstnameField);
            add(new JLabel("Patronymic:"));
            JTextField patrField = new JTextField();
            add(patrField);
            add(new JLabel("Age:"));
            JTextField ageField = new JTextField();
            add(ageField);
            add(new JLabel("Sex:"));
            JTextField sexField = new JTextField();
            add(sexField);
            add(new JLabel("Approximate age:"));
            JTextField apprAgeField = new JTextField();
            add(apprAgeField);
            add(new JLabel("Approximate height:"));
            JTextField apprHeightField = new JTextField();
            add(apprHeightField);
            add(new JLabel("Hair color:"));
            JTextField hairField = new JTextField();
            add(hairField);
            add(new JLabel("Special signs:"));
            JTextField ssField = new JTextField();
            add(ssField);
            add(new JLabel("Leaving date(yyyy-mm-dd):"));
            JFormattedTextField ldField = new JFormattedTextField(
                    new SimpleDateFormat("yyyy-mm-dd"));
            add(ldField);
            add(new JLabel("Leaving reason:"));
            JTextField lrField = new JTextField();
            add(lrField);
            JButton button = new JButton("Update the patient");
            add(button);

            button.addActionListener((e) -> {
                String sql = "UPDATE patient SET id_patient="
                        + pidField.getText();
                if (!entrDateField.getText().isEmpty())
                    sql += ", entr_date='" + entrDateField.getText() + "'";
                if (!entrWayField.getText().isEmpty())
                    sql += ", entr_way='" + entrWayField.getText() + "'";
                if (!surnameField.getText().isEmpty())
                    sql += ", surname='" + surnameField.getText() + "'";
                if (!firstnameField.getText().isEmpty())
                    sql += ", firstname='" + firstnameField.getText() + "'";
                if (!patrField.getText().isEmpty())
                    sql += ", patronymic='" + patrField.getText() + "'";
                if (!ageField.getText().isEmpty())
                    sql += ", age=" + ageField.getText();
                if (!sexField.getText().isEmpty())
                    sql += ", sex='" + sexField.getText() + "'";
                if (!apprAgeField.getText().isEmpty())
                    sql += ", appr_age=" + apprAgeField.getText();
                if (!apprHeightField.getText().isEmpty())
                    sql += ", appr_height=" + apprHeightField.getText();
                if (!hairField.getText().isEmpty())
                    sql += ", hair_color='" + hairField.getText() + "'";
                if (!ssField.getText().isEmpty())
                    sql += ", special_signs='" + ssField.getText() + "'";
                if (!ldField.getText().isEmpty())
                    sql += ", leaving_date='" + ldField.getText() + "'";
                if (!lrField.getText().isEmpty())
                    sql += ", leaving_reason=" + lrField.getText();
                sql += " WHERE id_patient=" + pidField.getText() + ";";

                Connector.INSTANCE.executeUpdate(sql);
                updateTable();
                this.dispose();
            });

            setVisible(true);
        }
    }

    @SuppressWarnings("serial")
    private class DeleteDialog extends JDialog {

        public DeleteDialog() {
            setTitle("Delete the patient");
            setModal(true);
            setSize(200, 105);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            setLocationRelativeTo(null);

            add(new JLabel("Patient ID:"));
            JTextField pidField = new JTextField();
            add(pidField);
            JButton button = new JButton("Delete the patient");
            add(button);

            button.addActionListener((e) -> {
                ResultSet rs = Connector.INSTANCE.executeQuery("SELECT ward_number FROM patient WHERE id_patient=" + pidField.getText() + ";");
                try {
                    while(rs.next()){
                        if(rs.getInt("ward_number") != 0){
                            Connector.INSTANCE.execute("DELETE FROM transfer WHERE id_patient="
                                    + pidField.getText() + ";");
                        }
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                Connector.INSTANCE.execute("DELETE FROM patient WHERE id_patient="
                        + pidField.getText() + ";");
                updateTable();
                this.dispose();
            });

            setVisible(true);
        }
    }
}
