package view;

import controller.TransferControl;

@SuppressWarnings("serial")
public class TransferPane extends Tab {
    
    public TransferPane(){
        this.columnIdentifiers = new Object[] {"Transfer date", "Ward number", "Patient ID"};
        this.tabControl = new TransferControl(pagComp);
        setPagCompAndControlPane();
    }
}
