package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import controller.TabController;

@SuppressWarnings("serial")
public abstract class Tab extends JPanel {
    protected PaginalComponent pagComp = new PaginalComponent();
    protected TabController tabControl;
    protected Object[] columnIdentifiers;

    protected void setPagCompAndControlPane(){
        pagComp.setColumnIdentifiers(columnIdentifiers);
        pagComp.setItemList(tabControl.getList());
        pagComp.init();
        this.setLayout(new BorderLayout());
        this.add(pagComp, BorderLayout.CENTER);
        this.add(createControlPane(), BorderLayout.NORTH);
    }
    
    private JPanel createControlPane(){
        JPanel panel = new JPanel();
        ImageIcon icon = new ImageIcon("reload.png");
        JButton addButton = new JButton("Add"),
                updateButton = new JButton("Update"),
                deleteButton = new JButton("Delete"),
                reloadButton = new JButton(icon);
        
        addButton.addActionListener((e)->{
            tabControl.add();
        });
        updateButton.addActionListener((e)->{
            tabControl.update();
        });
        deleteButton.addActionListener((e)->{
            tabControl.delete();
        });
        reloadButton.addActionListener((e)->{
            tabControl.updateTable();
        });
                
        panel.setLayout(new FlowLayout());
        panel.add(addButton);
        panel.add(updateButton);
        panel.add(deleteButton);
        panel.add(reloadButton);
        return panel;
    }
}
