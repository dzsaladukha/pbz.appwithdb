package controller;

import java.util.LinkedList;
import java.util.List;

import model.Entity;
import view.PaginalComponent;

public class PaginalComponentController {

    private List<Entity> pageElems = new LinkedList<Entity>();
    private List<Entity> itemList;
    private PaginalComponent pagComp;
    private Integer countElems, curIndex = 0, curPage = 1;

    public PaginalComponentController(PaginalComponent paginalComponent) {
        this.pagComp = paginalComponent;
        this.itemList = pagComp.getItemList();
        this.countElems = Integer.parseInt(pagComp.numOnPage.getText());
    }

    public void toFirstPage() {

        curPage = 1;
        curIndex = 0;
        combainPageElems(countElems);
        pagComp.updateTable(pageElems);
        pagComp.pageNum.setText("Страница 1");
    }

    public void toPrevPage() {

        curPage--;
        if (curPage < 2) {
            toFirstPage();
            return;
        }
        curIndex = (curPage - 1) * countElems;

        combainPageElems(countElems);
        pagComp.updateTable(pageElems);
        pagComp.pageNum.setText("Страница " + curPage);
    }

    public void toNextPage() {

        curPage++;
        if (curPage > itemList.size() / countElems) {
            toLastPage();
            return;
        }
        curIndex = (curPage - 1) * countElems;

        combainPageElems(countElems);
        pagComp.updateTable(pageElems);
        pagComp.pageNum.setText("Страница " + curPage);
    }

    public void toLastPage() {

        if (itemList.size() % countElems == 0) {
            curPage = itemList.size() / countElems;
        } else {
            curPage = itemList.size() / countElems + 1;
        }
        curIndex = (curPage - 1) * countElems;

        combainPageElems(itemList.size() - curIndex);
        pagComp.updateTable(pageElems);
        pagComp.pageNum.setText("Страница " + curPage);
    }

    public void combainPageElems(int length) {

        pageElems.clear();
        for (int index = curIndex; index < (curIndex + length); index++) {
            pageElems.add(itemList.get(index));
        }
    }

    public void changeCountOfVisibleElem() {

        if (itemList.size() >= Integer.parseInt(pagComp.numOnPage.getText())) {
            countElems = Integer.parseInt(pagComp.numOnPage.getText());

            toFirstPage();
        }
    }
}
