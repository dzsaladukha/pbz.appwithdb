package model;

public class Ward implements Entity{
    private Integer number;
    private String phone;
    
    public Integer getNumber() {
        return number;
    }
    public void setNumber(Integer number) {
        this.number = number;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    @Override
    public String[] toRow() {
        return new String[] { number.toString(), phone};
    }
}
