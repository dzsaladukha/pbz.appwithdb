package model;


public class Patient implements Entity {
    private Integer id_patient;
    private String entr_date;
    private String entr_way;
    private String surname;
    private String firstname;
    private String patronymic;
    private Integer age;
    private String sex;
    private Integer appr_age;
    private Integer appr_height;
    private String hair_color;
    private String special_signs;
    private String leave_date;
    private String leave_reason;
    private Integer ward_number;

    public Integer getPatientID() {
        return id_patient;
    }

    public void setPatientID(Integer id_patient) {
        this.id_patient = id_patient;
    }

    public String getEntrDate() {
        return entr_date;
    }

    public void setEntrDate(String entr_date) {
        this.entr_date = entr_date;
    }

    public String getEntrWay() {
        return entr_way;
    }

    public void setEntrWay(String entr_way) {
        this.entr_way = entr_way;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getApprAge() {
        return appr_age;
    }

    public void setApprAge(Integer appr_age) {
        this.appr_age = appr_age;
    }

    public Integer getApprHeight() {
        return appr_height;
    }

    public void setApprHeight(Integer appr_height) {
        this.appr_height = appr_height;
    }

    public String getHairColor() {
        return hair_color;
    }

    public void setHairColor(String hair_color) {
        this.hair_color = hair_color;
    }

    public String getSpecialSigns() {
        return special_signs;
    }

    public void setSpecialSigns(String string) {
        this.special_signs = string;
    }

    public String getLeaveDate() {
        return leave_date;
    }

    public void setLeaveDate(String string) {
        this.leave_date = string;
    }

    public String getLeaveReason() {
        return leave_reason;
    }

    public void setLeaveReason(String leave_reason) {
        this.leave_reason = leave_reason;
    }

    public Integer getWardNumber() {
        return ward_number;
    }

    public void setWardNumber(Integer ward_number) {
        this.ward_number = ward_number;
    }

    @Override
    public String[] toRow() {
        String fullname = null, ageStr = null, appr_ageStr = null, appr_heightStr = null, ward_numberStr = null, leave_dateStr = null;

        if (surname != null) {
            fullname = surname + " ";
        }
        if (firstname != null) {
            fullname += firstname + " ";
        }
        if (patronymic != null) {
            fullname += patronymic;
        }
        if (age != null) {
            if (age > 0) {
                ageStr = age.toString();
            }
        }
        if (appr_age != null) {
            if (appr_age > 0) {
                appr_ageStr = appr_age.toString();
            }
        }
        if (appr_height != null) {
            if (appr_height > 0) {
                appr_heightStr = appr_height.toString();
            }
        }
        if (ward_number != null) {
            if (ward_number > 0) {
                ward_numberStr = ward_number.toString();
            }
        }
        if (leave_date == null) {
            leave_dateStr = "on treatment";
        } else {
            leave_dateStr = leave_date;
        }

        return new String[] { id_patient.toString(), entr_date, entr_way,
                fullname, ageStr, sex, appr_ageStr, appr_heightStr, hair_color,
                special_signs, leave_dateStr, leave_reason, ward_numberStr };
    }
}
