package model;

import java.sql.Date;

public class Transfer implements Entity{
    private Date transfer_date;
    private Integer number;
    private Integer id_patient;
    
    public Date getTransferDate() {
        return transfer_date;
    }
    public void setTransferDate(Date transfer_date) {
        this.transfer_date = transfer_date;
    }
    public Integer getNumber() {
        return number;
    }
    public void setNumber(Integer number) {
        this.number = number;
    }
    public Integer getPatientID() {
        return id_patient;
    }
    public void setPatientID(Integer id_patient) {
        this.id_patient = id_patient;
    }
    @Override
    public String[] toRow() {
        return new String[] { transfer_date.toString(), number.toString(), id_patient.toString()};
    }
}
